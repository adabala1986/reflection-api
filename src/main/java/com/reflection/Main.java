package main.java.com.reflection;

import java.util.Scanner;
import java.lang.reflect.*;
import java.io.IOException;
import java.lang.Class;

public class Main {
	
	
	public void Menu(String classobj) throws IOException
	{	
		
		Scanner in = new Scanner(System.in);
		String temp;
		String menu_choice;		
		
		
		//do{
		this.printMenu();
		temp = in.next();
		menu_choice = temp.toLowerCase();
		ClassInfo clsobj=new ClassInfo(classobj);
		
		switch (menu_choice)
		{
		case "methods":	
			try {
			clsobj.getMethod();
			}catch (Exception e){
	            e.printStackTrace();
	        }
			break;
			
		case "class":
			clsobj.getClassName();
			break;
			
		case "subclasses":
			clsobj.getSubClass();
			break;
			
		case "parentclasses":
			clsobj.getParentClass();
			
			break;
			
		case "constructors":
			clsobj.getConstructor();
			break;
			
		case "datamembers":
			clsobj.getDataMember();
			break;
	
		case "quit":
			break;			
	
		}
		//}while (menu_choice != "Quit");

}	
	
	public void printMenu()
	{	
		System.out.println("Select from the below menu:\n");
		System.out.println("Methods");
		System.out.println("Class");
		System.out.println("Subclasses");
		System.out.println("Parentclasses");
		System.out.println("Constructors");
		System.out.println("DataMembers");
		System.out.println("Quit");
	}

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		Main obj= new Main();
		obj.Menu(args[0]);

	}

}
