package main.java.com.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.Class;
import java.io.*;


public class ClassInfo {
	public String name;

	public ClassInfo(String name) {
		super();
		this.name = name;
	}
	
	
public void getMethod() throws IOException
{	
	Class cls =name.getClass();	
	Method[] methods = cls.getMethods();			  
    // Printing method names
    for (Method method:methods)
    {
       printtoFile(method.getName());
    }

}

public void getConstructor() throws IOException
{
	Class cls =name.getClass();	
	System.out.println("The class name is :"+cls.getName());
	Constructor [] constructor = cls.getConstructors();		
    
	// Printing constructor names
    for (Constructor constructor1:constructor)
    {
        printtoFile(constructor1.getName());
    }
}

public void getClassName() throws IOException
{

	Class cls =name.getClass();	
	System.out.println("The class name is :"+cls.getName());
	printtoFile(cls.getName());
}

public void getDataMember() throws IOException
{
	Class cls =name.getClass();	
	Field[] fields = cls.getFields();
	for(Field field : fields) 
	{
		 System.out.println(field.getName());
		 printtoFile(field.getName());
	}
	
}

public void getSubClass() throws IOException
{
	Class cls =name.getClass();
	Class<?>[] subClasses=cls.getSuperclass().getClasses();
	System.out.println( subClasses.getClass().getName());
	 printtoFile(subClasses.getClass().getName());
		 
	 
}
public void getParentClass() throws IOException
{
	Class cls =name.getClass();
	Class<?>[] subClasses=cls.getSuperclass().getClasses();
	System.out.println( subClasses.getClass().getName());
	 printtoFile(subClasses.getClass().getName());
}

public void printtoFile(String data) throws IOException
{
	BufferedWriter out = new BufferedWriter(new FileWriter("output.txt",true));
	out.write(data);
	out.write("\n");
    // Closing the connection
    out.close();
}
}
